//
//  ViewController.swift
//  TokenShow
//
//  Created by Joel Rennich on 12/8/17.
//  Copyright © 2017 Joel Rennich. All rights reserved.
//

import Cocoa
import CryptoTokenKit
import SecurityInterface.SFCertificatePanel
import os.log

let kNotificationRemove = "menu.nomad.tokenshow.tokenremove"

class ViewController: NSViewController {
    
    // IB Outlets
    
    @IBOutlet weak var tableView: NSTableView!
    @IBOutlet weak var certButton: NSButton!
    @IBOutlet weak var iDPButton: NSButton!
    @IBOutlet weak var exportButton: NSButton!
    @IBOutlet weak var changeButton: NSButton!
    @IBOutlet weak var attemptsField: NSTextField!
    @IBOutlet weak var infoButton: NSButton!
    
    // Globals
    
    var myTKWatcher: TKTokenWatcher? = nil
    let keychain = KeychainUtil()
    var identities : [identityList]? = nil
    var currentRow : Int = 0
    private static let log = OSLog(subsystem: "menu.nomad.TokenShow", category: "App")
    
    // CTK Token Watcher bits
    
    typealias handler = (String) -> Swift.Void
    let myHandler: handler = { tokenID in
        DispatchQueue.main.async {
            NotificationQueue.default.enqueue( Notification(name: Notification.Name(rawValue: kNotificationRemove), object: nil), postingStyle: .now, coalesceMask: .onName, forModes: nil)
        }
    }
    
    // APDU things
    
    let apid : [UInt8] = [0x00, 0xa4, 0x04, 0x00, 0x0b, 0xa0, 0x00, 0x00, 0x03, 0x08, 0x00, 0x00, 0x10, 0x00, 0x01, 0x00 ]
    
    // PEM export functions
    
    private let kCryptoExportImportManagerPublicKeyInitialTag = "-----BEGIN RSA PUBLIC KEY-----\n"
    private let kCryptoExportImportManagerPublicKeyFinalTag = "-----END RSA PUBLIC KEY-----\n"
    
    private let kCryptoExportImportManagerRequestInitialTag = "-----BEGIN CERTIFICATE REQUEST-----\n"
    private let kCryptoExportImportManagerRequestFinalTag = "-----END CERTIFICATE REQUEST-----\n"
    
    private let kCryptoExportImportManagerPublicNumberOfCharactersInALine = 64
    
    func PEMKeyFromDERKey(_ data: Data, PEMType: String) -> String {
        
        var resultString: String
        
        // base64 encode the result
        let base64EncodedString = data.base64EncodedString(options: [])
        
        // split in lines of 64 characters.
        var currentLine = ""
        if PEMType == "RSA" {
            resultString = kCryptoExportImportManagerPublicKeyInitialTag
        } else {
            resultString = kCryptoExportImportManagerRequestInitialTag
        }
        var charCount = 0
        for character in base64EncodedString {
            charCount += 1
            currentLine.append(character)
            if charCount == kCryptoExportImportManagerPublicNumberOfCharactersInALine {
                resultString += currentLine + "\n"
                charCount = 0
                currentLine = ""
            }
        }
        // final line (if any)
        if currentLine.count > 0 { resultString += currentLine + "\n" }
        // final tag
        if PEMType == "RSA" {
            resultString += kCryptoExportImportManagerPublicKeyFinalTag
        } else {
            resultString += kCryptoExportImportManagerRequestFinalTag
        }
        return resultString
    }
    
    // NSView overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        logIt("Starting TokenShow")
        
        myTKWatcher = TKTokenWatcher.init(insertionHandler: { tokenID in
            self.logIt("Token inserted: \(tokenID)")
            self.checkTokens()
        })
        
        tableView.delegate = self
        tableView.dataSource = self
        
        // set up a notification listener
        
        NotificationCenter.default.addObserver(self, selector: #selector(update), name: NSNotification.Name(rawValue: kNotificationRemove), object: nil)
        update()
        NSApp.mainWindow?.center()
    }
    
    override func viewDidAppear() {
        
    // disabled expiration check for App Store version
        //checkExpiration()

    }
    
    override func viewWillDisappear() {
        NSApp.terminate(nil)
    }
    
    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    func checkTokens() {
        if ( myTKWatcher?.tokenIDs.count ?? 0 ) > 0 {
            for id in (myTKWatcher?.tokenIDs)! {
                logIt("Token here: \(id)")
                myTKWatcher?.addRemovalHandler(myHandler, forTokenID: id)
                
                identities = keychain.findIdentities(tokensOnly: 1)
                
                self.findAttempts()
                
                RunLoop.main.perform {
                    self.tableView.reloadData()
                    if self.identities?.count == 0 {
                        self.iDPButton.isEnabled = false
                        self.certButton.isEnabled = false
                        self.exportButton.isEnabled = false
                        self.changeButton.isEnabled = false
                        self.attemptsField.isEnabled = false
                        return
                    } else {
                        self.iDPButton.isEnabled = true
                        self.certButton.isEnabled = true
                        self.exportButton.isEnabled = true
                        self.changeButton.isEnabled = true
                        self.attemptsField.isEnabled = true
                    }
                }
            }
        }
    }
    
    @objc func update() {
        // update identity list
        
        identities = keychain.findIdentities(tokensOnly: 1)
        
        RunLoop.main.perform {
            self.tableView.reloadData()
            
            self.findAttempts()
            
            if self.identities?.count == 0 {
                self.iDPButton.isEnabled = false
                self.certButton.isEnabled = false
                self.exportButton.isEnabled = false
                self.changeButton.isEnabled = false
                self.attemptsField.stringValue = "Attempts: "
                self.attemptsField.isEnabled = false
            } else {
                self.iDPButton.isEnabled = true
                self.certButton.isEnabled = true
                self.exportButton.isEnabled = true
                self.changeButton.isEnabled = true
                self.attemptsField.isEnabled = true
            }
        }
        
        for id in identities! {
            logIt("Token:")
            logIt("  \(String(describing: id.principal))")
            logIt("  \(id.pubKeyHash)")
            logIt("")
        }
    }
    
    // actions
    
    @IBAction func clickCertView(_ sender: Any) {
        
        if !sanityCheck() {
            return
        }
        
        var secRef: SecCertificate? = nil

        let certRefErr = SecIdentityCopyCertificate(identities![currentRow].identity, &secRef)
        
        if certRefErr == 0 {
            
            SFCertificatePanel.shared()?.beginSheet(for: NSApp.windows.first, modalDelegate: nil, didEnd: nil, contextInfo: nil, certificates: [secRef!], showGroup: true)
        //SFCertificatePanel.shared()?.runModal(forCertificates: [secRef!], showGroup: true)
            
        } else {
            print("ERROR: getting certificate to show trust")
        }
    }
    
    @IBAction func clickSetIDP(_ sender: Any) {
        
        if !sanityCheck() {
            return
        }
        
        // show panel to get identity to set
        
        let alert = NSAlert()
        alert.messageText = "Add an e-mail, URI, DNS name or other identifier to set an Identity Preference for \(identities![currentRow].cn)"
        alert.addButton(withTitle: "Create")
        alert.addButton(withTitle: "Cancel")
        
        let prefID = NSTextField(frame: CGRect(x: 0, y: 0, width: 200, height: 24))
        alert.accessoryView = prefID
        prefID.becomeFirstResponder()
        
        alert.beginSheetModal(for: NSApp.mainWindow!, completionHandler: { [unowned self] (returnCode) -> Void in
            if ( returnCode == NSApplication.ModalResponse.alertFirstButtonReturn ) && prefID.stringValue != "" {
                SecIdentitySetPreferred(self.identities![self.currentRow].identity, prefID.stringValue as CFString, nil)
            }
        })
    }
    
    @IBAction func clickExport(_ sender: Any) {
        
        if !sanityCheck() {
            return
        }
        
        var pem : String? = nil
        let id = identities![self.currentRow].identity
        var publicCert: SecCertificate? = nil
        
        let err = SecIdentityCopyCertificate(id, &publicCert)
        
        if err == 0 {
            let certData = SecCertificateCopyData(publicCert!)
            pem = PEMKeyFromDERKey(certData as Data, PEMType: "RSA")
        } else {
            logIt("Error getting public certificate.")
            return
        }
        
        // Save dialog box
        
        let savePanel = NSSavePanel()
        savePanel.title = "Save a PEM file."
        savePanel.message = "Choose where to save your PEM file:"
        savePanel.prompt = "Save PEM"
        savePanel.canCreateDirectories = true
        savePanel.allowedFileTypes = [ "cer" ]
        savePanel.allowsOtherFileTypes = true
        
        savePanel.beginSheetModal(for: NSApp.mainWindow!, completionHandler: { response in
            
            if response.rawValue != 0 {
            
            do {
                try pem?.write(to: savePanel.url!, atomically: true, encoding: String.Encoding.utf8)
            } catch {
                
                return
            }
            }
        })
    }
    
    @IBAction func changePINClick(_ sender: Any) {
        
        //getCerts()
        
        //return
        
        let sm = TKSmartCardSlotManager()
        let slots = sm.slotNames

        if slots.count > 1 {
            RunLoop.main.perform {
                let alert = NSAlert()
                alert.messageText = "Mulitple cards present, please only use 1"
                alert.beginSheetModal(for: self.view.window!, completionHandler: nil)
            }
            
        } else {
        
        let storyBoard = NSStoryboard(name: NSStoryboard.Name(rawValue: "Main"), bundle: nil)  as NSStoryboard
        let myViewController = storyBoard.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "PINChange")) as! NSViewController
        self.presentViewControllerAsSheet(myViewController)
        }
    }
    
    // utility functions
    
    func sanityCheck() -> Bool {
        
        currentRow = tableView.selectedRow
        
        if currentRow == -1 {
            return false
        }
        
        return true
    }
    
    func logIt(_ message: String) {
        os_log("%{public}@", log: ViewController.log, type: .default, message)
    }
    
    // find out how many PIN attempts are left
    
    // 1. ensure a card is there
    // 2. start a session with card
    // 3. send APID to card to get PIV applet
    // 4. send empty PIN verify request to card
    // 5. look at result - first byte "63" is success, second byte "cx" where x is hex number of attempts remaining
    
    func findAttempts() {
        let sm = TKSmartCardSlotManager()
        let slots = sm.slotNames.filter { $0 != "TCS Virtual Serial" }
        var card : TKSmartCard? = nil
        
        let sema = DispatchSemaphore.init(value: 0)
        
        print(slots.debugDescription)
        
        if slots.count > 1 {
            RunLoop.main.perform {
                self.attemptsField.stringValue = "Attempts: multiple tokens"
            }
            return
        }
        
        if slots.count < 1 {
            return
        }
        
        for slot in slots {
            
            sm.getSlot(withName: slot, reply: { currentslot in
                //print("Slot:")
                //print(currentslot as Any)
                card = currentslot?.makeSmartCard()
                sema.signal()
            })
            sema.wait()
        }
        
        // If there is no card present, continue
        if card == nil {
            return
        }
        
        card?.beginSession(reply: { something, error in
            
            let pinVerifyNull : [UInt8] = [ 0x00, 0x20, 0x00, 0x80, 0x00]
            
            let apidRequest = Data.init(bytes: self.apid)
            let request2 = Data.init(bytes: pinVerifyNull)
            
            card?.transmit(apidRequest, reply: { data, error in
                if error == nil {
                    
                    card?.transmit(request2, reply: { data, error in
                        //print("Send:")
                        let result = data!.hexEncodedString()
                        
                        // convert from hex to decimal
                        
                        let attempts = Int(String(result.last!), radix: 16)
                        
                        var attemptsText = ""
                        
                        // if attempts left == 0, card is locked
                        // otherwise print attempts
                        // unless we didn't get a success code
                        
                        if attempts == 0 {
                            attemptsText = "locked"
                        } else {
                            attemptsText = (attempts?.description)!
                        }
                        
                        //print(data!.hexEncodedString())
                        
                        // check for "63" in the sequence
                        // TODO: check just first two words
                        
                        if !String(describing: data!.hexEncodedString()).contains("63") {
                            attemptsText = "Locked"
                        }
                        
                        RunLoop.main.perform {
                            self.attemptsField.stringValue = "Attempts: \(attemptsText)"
                        }
                        
                        sema.signal()
                    })
                } else {
                    RunLoop.main.perform {
                        self.attemptsField.stringValue = "Attempts: "
                    }
                    //print("Error")
                    sema.signal()
                }
            })
            
        })
        sema.wait()
        
        // be nice and end the session
        
        card?.endSession()
    }
    
    func getCerts() {
        
        let sm = TKSmartCardSlotManager()
        let slots = sm.slotNames
        var card : TKSmartCard? = nil
        
        let sema = DispatchSemaphore.init(value: 0)
        
        if slots.count > 1 {
            RunLoop.main.perform {
                self.attemptsField.stringValue = "Attempts: multiple tokens"
            }
            return
        }
        
        if slots.count < 1 {
            return
        }
        
        for slot in slots {
            sm.getSlot(withName: slot, reply: { currentslot in
                //print("Slot:")
                //print(currentslot as Any)
                card = currentslot?.makeSmartCard()
                sema.signal()
            })
            sema.wait()
        }
        
        card?.beginSession(reply: { something, error in
            
//            let pinVerifyNull : [UInt8] = [ 0x00, 0x20, 0x00, 0x80, 0x00]
            let odf : [UInt8] = [ 0x00, 0xa4, 0x00, 0x0c, 0x02, 0x50, 0x31, 0x00, 0xb0, 0x00, 0x00, 0x3c]
            let apidRequest = Data.init(bytes: self.apid)
            let odfRequest = Data.init(bytes: odf)
//            let request2 = Data.init(bytes: pinVerifyNull)

            card?.transmit(apidRequest, reply: { data, error in
                
                if error == nil {
                    card?.transmit(odfRequest, reply: { data ,error in
                        //print("ODF Response")
                        //print(data?.hexEncodedString())
                        sema.signal()
                    })
                }
            })
        })
        
        sema.wait()
    }
    
    // quick check for Expiration
    
    func checkExpiration() {
        
        if self.view.window == nil {
            logIt("We shouldn't be here... no window to attach to.")
            NSApp.terminate(nil)
        }
        
        let df = DateFormatter.init()
        df.dateStyle = .medium
        
        let expire = Date(timeIntervalSinceReferenceDate: 539071200 ) //528454400) // Dec. 29
        let now = Date()
        if now.timeIntervalSince(expire) > 0 {
            logIt("Expired! You should check for an update.")
            let myAlert = NSAlert()
            myAlert.messageText = "This beta copy of Token Show has expired, please contact sales@nomad.menu for an updated version."
            myAlert.beginSheetModal(for: self.view.window!, completionHandler: { response in
                NSApp.terminate(nil)
            })
        } else {
            self.logIt("This version of Token Show will expire on: " + df.string(from: expire))
            let myAlert = NSAlert()
            myAlert.messageText = "This beta copy of Token Show will expire on " + df.string(from: expire) + "."
            myAlert.beginSheetModal(for: self.view.window!, completionHandler: nil)
        }
    }
    
    @IBAction func clickInfo(_ sender: Any) {
        
        //print("DONE")
    }
    
}

