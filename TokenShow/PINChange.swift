//
//  PINChange.swift
//  TokenShow
//
//  Created by Joel Rennich on 12/10/17.
//  Copyright © 2017 Joel Rennich. All rights reserved.
//

import Foundation
import Cocoa
import CryptoTokenKit

class PINChange : NSViewController {
    @IBOutlet weak var current: NSSecureTextFieldCell!
    @IBOutlet weak var newPIN: NSSecureTextField!
    @IBOutlet weak var verifyPIN: NSSecureTextField!
    @IBOutlet weak var changeButton: NSButton!
    @IBOutlet weak var cancelButton: NSButtonCell!
    
    // APDU things
    
    let apid : [UInt8] = [0x00, 0xa4, 0x04, 0x00, 0x0b, 0xa0, 0x00, 0x00, 0x03, 0x08, 0x00, 0x00, 0x10, 0x00, 0x01, 0x00 ]
    
    @IBAction func clickChangeButton(_ sender: Any) {
        
        RunLoop.main.perform {
        // Resetting PIN
        
        // validate newPIN and verifyPIN are the same
        
        if self.newPIN.stringValue != self.verifyPIN.stringValue {
            let alert = NSAlert()
            alert.messageText = "PINS don't match."
            alert.runModal()
            return
        }
            
        let currentPinData = self.current.stringValue.data(using: String.Encoding.ascii)
        let newPinData = self.verifyPIN.stringValue.data(using: String.Encoding.ascii)

        
        // 1. ensure a card is present
        // 2. send APID to card
        // 3. send PIN verify to card
        // 4. select the file to modify
        // 5. send modify PIN to card with new PIN and old PIN
        
        let sm = TKSmartCardSlotManager()
        let slots = sm.slotNames
        var card : TKSmartCard? = nil
        
        let sema = DispatchSemaphore.init(value: 0)
        
        if slots.count < 1 {
            // no cards, we shouldn't be here
            return
        }
        
        for slot in slots {
            sm.getSlot(withName: slot, reply: { currentslot in
                //print("Slot:")
                //print(currentslot as Any)
                card = currentslot?.makeSmartCard()
                sema.signal()
            })
        }
        
        sema.wait()
        
        card?.beginSession(reply: { something, error in
            //print(something)
            //print(error as Any)
            
            let apidRequest = Data.init(bytes: self.apid)
            
            // set up padding on hte main loop since we're accessing UI
            
            let pad : [UInt8] = [0xff]
            let padData = Data.init(bytes: pad)
            
            //00 20 00 80 08
            let pinVerify : [UInt8] = [ 0x00, 0x20, 0x00, 0x80, 0x08 ]
            
            var pinVerifyRequest = Data.init(bytes: pinVerify)
            pinVerifyRequest.append(currentPinData!)
            
            // pad it out
            
            if currentPinData!.count < 8 {
                for _ in currentPinData!.count...( 8 - 1 ){
                    pinVerifyRequest.append(padData)
                }
            }
            
            // select file to modify = 00 a4 04 00 05 a0 00 00 03 08
            let selectFile : [UInt8] = [0x00, 0xa4, 0x04, 0x00, 0x05, 0xa0, 0x00, 0x00, 0x03, 0x08 ]
            
            let selectModifyRequest = Data.init(bytes: selectFile)
            
            // 00 24 00 80 10 
            let pinChange : [UInt8] = [ 0x00, 0x24, 0x00, 0x80, 0x10 ]
            
            var pinChangeRequest = Data.init(bytes: pinChange)
            
            // Add and pad the current PIN
            
            pinChangeRequest.append(currentPinData!)
            
            if currentPinData!.count < 8 {
                for _ in currentPinData!.count...( 8 - 1 ){
                    pinChangeRequest.append(padData)
                }
            }
            
            // Add and pad the new PIN
            
            pinChangeRequest.append(newPinData!)

            if newPinData!.count < 8 {
                for _ in newPinData!.count...( 8 - 1 ){
                    pinChangeRequest.append(padData)
                }
            }
            
            card?.transmit(apidRequest, reply: { data, error in
                //print(error as Any)
                //print(data![0].words)
                //print(data![1].words)
                
                if error == nil {
                    
                    card?.transmit(pinVerifyRequest, reply: { data, error in
                        if error == nil {
                            //print("Verify Request")
//                            let result = data!.hexEncodedString()
                            //print(result)
                            
                            // validate PIN
                            
                            if String(describing: data!.hexEncodedString()) != "9000" {
                                // PIN was incorrect
                                //print("Incorrect PIN")
                                return
                            }
                            
                            //print("Attempts remaining: \(result.characters.last)")
                            //print(data![0].words)
                            //print(data![1].words)
                            
                            card?.transmit(selectModifyRequest, reply: { data, error in
                                //print("Send:")
//                                let result = data!.hexEncodedString()
                                //print(result)
                                
                                //print("Attempts remaining: \(result.characters.last)")
                                //print(data![0].words)
                                //print(data![1].words)
                                
                                if error == nil {
                                    card?.transmit(pinChangeRequest, reply: { data, error in
//                                        let result = data!.hexEncodedString()
                                        //print(result)
                                        
                                        //print(data![0].words)
                                        //print(data![1].words)
                                        sema.signal()
                                    })
                                } else {
                                    //print("Error")
                                    //print(error as Any)
                                    sema.signal()
                                }
                            })
                        } else {
                            //print("Error")
                            //print(error as Any)
                            sema.signal()
                        }
                    })
                } else {
                    //print("Error")
                    //print(error as Any)
                    sema.signal()
                }
            })
        })
        
        sema.wait()
        
        card?.endSession()
        }
        self.view.window?.close()
    }
    
    @IBAction func clickCancel(_ sender: Any) {
        self.view.window?.close()
    }
    
}
